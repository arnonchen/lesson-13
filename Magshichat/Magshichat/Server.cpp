#include "Server.h"
#include <exception>
#include <iostream>
#include <string>
#include "Helper.h"
#include <algorithm>
#include <mutex>
#include <fstream>

#define  _CRT_SECURE_NO_WARNINGS
std::mutex m;
Server::Server()
{
	_serverSocket = socket(AF_INET, SOCK_STREAM, IPPROTO_TCP);
	if (_serverSocket == INVALID_SOCKET)
		throw std::exception(__FUNCTION__ " - socket");
}

Server::~Server()
{
	try
	{
		closesocket(_serverSocket);
	}
	catch (...) {}
}

void Server::serve(int port)
{

	struct sockaddr_in sa = { 0 };

	sa.sin_port = htons(8826); 
	sa.sin_family = AF_INET;   
	sa.sin_addr.s_addr = INADDR_ANY;    

	if (bind(_serverSocket, (struct sockaddr*) & sa, sizeof(sa)) == SOCKET_ERROR)
		throw std::exception(__FUNCTION__ " - bind");

	if (listen(_serverSocket, SOMAXCONN) == SOCKET_ERROR)
		throw std::exception(__FUNCTION__ " - listen");
	std::cout << "Listening on port " << port << std::endl;
	while (true)
	{
		std::cout << "waiting for user to connect";
		accept();
	}
}


void Server::accept()
{
	SOCKET client_socket = ::accept(_serverSocket, NULL, NULL);

	if (client_socket == INVALID_SOCKET)
		throw std::exception(__FUNCTION__);
	std::cout << "Client accepted. Server and client can speak" << std::endl;
	std::thread t1(&Server::clientHandler, this, client_socket);
	t1.detach();
}


void Server::clientHandler(SOCKET clientSocket)
{
	std::vector<std::string> fileNames;
	std::string user;
	std::fstream file;

	try
	{
		while (true)
		{
			
			int message = Helper::getMessageTypeCode(clientSocket);
			switch (message)
			{
				case 200:
				{
					int length = Helper::getIntPartFromSocket(clientSocket, 2);
					user = Helper::getStringPartFromSocket(clientSocket, length);
					connectedUsers.insert(user);		
					Helper::send_update_message_to_client(clientSocket, "", "", stringedUsers());
					break;
				}
				case 204:
				{
					int length = Helper::getIntPartFromSocket(clientSocket, 2);
					if (length == 0)
					{
						Helper::getIntPartFromSocket(clientSocket, 5);
						Helper::send_update_message_to_client(clientSocket, "", "", stringedUsers());
					}
					else
					{
						std::string name2 = Helper::getStringPartFromSocket(clientSocket, length);
						int msgLen = Helper::getIntPartFromSocket(clientSocket, 5);
						fileNames.clear();
						fileNames.push_back(user);
						fileNames.push_back(name2);
						std::sort(fileNames.begin(), fileNames.end());
						std::string  fileName = fileNames[0] + "&" + fileNames[1] + ".txt";
						if (msgLen == 0)
						{
							m.lock();
							file.open(fileName, std::ios::in);
							std::string line;
							if (file.is_open())
							{
								std::getline(file, line);
								std::cout << line << std::endl;
								file.close();
							}
							m.unlock();
							Helper::send_update_message_to_client(clientSocket, line, name2, stringedUsers());
						}
						else
						{
							std::string message = Helper::getStringPartFromSocket(clientSocket, msgLen);
							m.lock();
							file.open(fileName, std::ios::out | std::ios::app);
							if (file.is_open())
							{
								file << "&MAGSH_MESSAGE&&Author&" << user << "&DATA&" << message;
								file.close();
							}
							m.unlock();
						}
					}
					break;
				}
				case 0:
				{
					std::cout << user << " has disconnected!" << std::endl;
					connectedUsers.erase(user);
					break;
				}
			}
		}
		closesocket(clientSocket);
	}
	catch (const std::exception & e)
	{
		std::cout << e.what() << std::endl;
		connectedUsers.erase(user);
		closesocket(clientSocket);
	}
}

std::string Server::stringedUsers()
{
	std::set<std::string>::iterator it = connectedUsers.begin();
	std::string names = *it;
	for (++it; it != connectedUsers.end(); it++)
	{
		names += "&";
		names += *it;
	}
	return names;
}