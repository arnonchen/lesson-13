#pragma once

#include <WinSock2.h>
#include <Windows.h>
#include <set>
#include <string>
class Server
{
public:
	Server();
	~Server();
	void serve(int port);
private:
	std::set<std::string> connectedUsers;
	std::string stringedUsers();
	void accept();
	void clientHandler(SOCKET clientSocket);

	SOCKET _serverSocket;
};